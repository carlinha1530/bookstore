# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table passageiro (
  id_passageiro             varchar(255) not null,
  nome                      varchar(255),
  cpf                       varchar(255),
  email                     varchar(255),
  telefone                  varchar(255),
  constraint pk_passageiro primary key (id_passageiro))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table passageiro;

SET FOREIGN_KEY_CHECKS=1;

