package model.dao;

import java.util.List;
import model.vo.Book;

public class BookDAO extends BaseDAO<Book> {
	
	public BookDAO() {
		super(Book.class);
	}

	
}