package controllers;


import model.dao.PassageiroDAO;
import model.vo.Passageiro;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.*;

public class Application extends Controller {

    public static Result getPassageiro() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new PassageiroDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result savePassageiro() throws Exception {
    	JsonNode json = request().body().asJson();
    	Passageiro c =  JsonObjectParser.Deserialize(json, Passageiro.class);
    	new PassageiroDAO().create(c);
		return ok("saved");
    }
    
}
